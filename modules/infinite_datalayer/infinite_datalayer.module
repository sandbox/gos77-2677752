<?php

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\taxonomy\Entity\Term;

/**
 * @file
 * Enriches data layer with information about nodes and terms.
 */

/**
 * Implements hook_node_view().
 */
function infinite_datalayer_node_view(array &$build, EntityInterface $node, EntityViewDisplayInterface $display, $view_mode) {
  if (!in_array($view_mode, ['full', 'lazyloading']) || $node->bundle() != 'article') {
    return;
  }

  $datalayer_variables = _infinite_datalayer_initialize_variables();
  $datalayer_variables['page'] = [
    'name' => $node->title->value,
    'contentType' => $node->bundle(),
    'contentSubType' => _infinite_datalayer_content_sub_types($node->field_paragraphs->referencedEntities()),
    'articlePublishDate' => date(DATE_ISO8601, $node->created->value),
    'authorName' => $node->uid->entity->getDisplayName(),
  ];

  if (isset($node->field_channel->entity)) {
    $datalayer_variables['page'] += _infinite_datalayer_category($node->field_channel->entity);
  }

  if (\Drupal::request()->attributes->get('js') == 'ajax') {
    $key = $node->uuid();
  }
  else {
    $key = 'page';
  }

  infinite_datalayer_add($build, $key, $datalayer_variables);
}

/**
 * Implements hook_taxonomy_term_view().
 */
function infinite_datalayer_taxonomy_term_view(array &$build, EntityInterface $term, EntityViewDisplayInterface $display, $view_mode) {
  if ($view_mode == 'amp' || !taxonomy_term_is_page($term)) {
    return;
  }

  $datalayer_variables = _infinite_datalayer_initialize_variables();
  $datalayer_variables['page'] = [
    'title' => $term->name->value,
    'contentType' => $term->bundle(),
  ];

  if ($term->bundle() == 'channel') {
    $datalayer_variables['page'] += _infinite_datalayer_category($term);
  }

  infinite_datalayer_add($build, 'page', $datalayer_variables);
}

/**
 * Implements hook_user_view().
 */
function infinite_datalayer_user_view(array &$build, EntityInterface $user, EntityViewDisplayInterface $display, $view_mode) {
  if ($view_mode == 'amp') {
    return;
  }

  if (\Drupal::routeMatch()->getRouteName() != 'entity.user.canonical') {
    return;
  }

  if (\Drupal::routeMatch()->getParameter('user')->id() != $user->id()) {
    return;
  }

  $datalayer_variables = _infinite_datalayer_initialize_variables();
  $datalayer_variables['page'] = [
    'title' => $user->getDisplayName(),
    'contentType' => $user->bundle(),
  ];

  infinite_datalayer_add($build, 'page', $datalayer_variables);
}

/**
 * Implements hook_preprocess_node().
 */
function infinite_datalayer_preprocess_node(&$variables) {
  $variables['uuid'] = $variables['node']->uuid();
}

/**
 * Adds datalayer variables to the html head and drupalSettings.
 *
 * @param array $build
 * @param string $key
 * @param array $variables
 */
function infinite_datalayer_add(&$build, $key, $variables) {
  $datalayer_object = json_encode($variables);

  $build['#attached']['html_head'][] = [
    [
      '#type' => 'html_tag',
      '#tag' => 'script',
      '#attributes' => ['type' => 'text/javascript'],
      '#value' => "dataLayer = [$datalayer_object];",
    ],
    'datalayer',
  ];
  $build['#attached']['library'][] = 'core/drupalSettings';
  $build['#attached']['drupalSettings']['datalayer'][$key] = $variables;
}

/**
 * Returns data layer object initialized with common data.
 *
 * @return array
 */
function _infinite_datalayer_initialize_variables() {
  return [
    'environment' => [
      'systemType' => isset($_ENV['AH_SITE_ENVIRONMENT']) ? $_ENV['AH_SITE_ENVIRONMENT'] : 'local',
      'isLoggedIn' => \Drupal::currentUser()->isAuthenticated(),
    ],
    'page' => [],
  ];
}

/**
 * Returns an ordered set of labels for eligible paragraphs.
 *
 * @param Paragraph[] $paragraphs
 *
 * @return array
 */
function _infinite_datalayer_content_sub_types(array $paragraphs) {
  $content_sub_types = [];

  foreach ($paragraphs as $paragraph) {
    if (in_array($paragraph->bundle(), ['gallery', 'instagram', 'pinterest', 'riddle', 'twitter', 'video'])) {
      $content_sub_types[] = Unicode::ucfirst($paragraph->bundle());
    }
    elseif ($paragraph->bundle() == 'tracdelight') {
      $content_sub_types[] = 'Affiliate-Tracdelight';
    }
    elseif ($paragraph->bundle() == 'advertising_products_paragraph') {
      foreach ($paragraph->field_advertising_products->referencedEntities() as $product) {
        if ($product->bundle() == 'advertising_product_amazon') {
          $content_sub_types[] = 'Affiliate-Amazon';
        }
        elseif ($product->bundle() == 'advertising_product_tracdelight') {
          $content_sub_types[] = 'Affiliate-Tracdelight';
        }
      }
    }
  }

  sort($content_sub_types);

  return array_values(array_unique($content_sub_types));
}

/**
 * Returns category and sub category derived from the given term.
 *
 * @param Term $term
 *   A channel term
 *
 * @return array
 *   An associative array with category and optional subCategory
 */
function _infinite_datalayer_category(Term $term) {
  $category = [];
  $parents = Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadAllParents($term->id());

  switch (count($parents)) {
    case 1:
      // The category should be Home or the second level of the channel
      // hierarchy. If the term is in the third level of the hierarchy or
      // deeper the second level of the hierarchy should be the category and
      // the term itself the sub category.
      //
      // Note: The array returned by TermStorage::loadAllParents also contains
      // the term itself and is ordered by term weight, hence the first level
      // term is in the last position.
    case 2:
      $category['category'] = $term->name->value;
      break;
    default:
      $parent = array_values($parents)[count($parents) - 2];
      $category['category'] = $parent->name->value;
      $category['subCategory'] = $term->name->value;
      break;
  }

  return $category;
}
