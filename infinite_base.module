<?php

/**
 * @file
 * Module for adding custom Infinity base functions.
 */

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\node\NodeInterface;

/**
 * Implements hook_entity_base_field_info_alter().
 */
function infinite_base_entity_base_field_info_alter(&$fields, EntityTypeInterface $entity_type) {
  if ($entity_type->id() == 'node' && !empty($fields['uid'])) {
    $fields['created']->setDisplayConfigurable('view', TRUE);
    $fields['uid']->setDisplayConfigurable('view', TRUE);
  }
  else if ($entity_type->id() == 'user' && !empty($fields['mail'])) {
    $fields['mail']->setDisplayConfigurable('view', TRUE);
  }
}

/**
 * Implements hook_entity_extra_field_info().
 */
function infinite_base_entity_extra_field_info() {
  $extra = [];

  $extra['user']['user']['display']['field_full_name'] = [
    'label' => t('Full name'),
    'description' => t('The full name of the user derived from relevant fields'),
    'visible' => TRUE,
  ];

  return $extra;
}

/**
 * Implements hook_node_view().
 */
function infinite_base_node_view(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
  if (isset($build['created'])) {
    $build['field_created'] = $build['created'];
  }

  if (isset($build['uid'])) {
    $build['field_uid'] = $build['uid'];
  }
}

/**
 * Implements hook_user_view().
 */
function infinite_base_user_view(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
  if ($display->getComponent('field_full_name')) {
    $field_full_name = '';

    if (isset($entity->field_forename->value)) {
      $field_full_name = $entity->field_forename->value;
    }
    else if (isset($entity->first_name->value)) {
      $field_full_name = $entity->first_name->value;
    }
    if (isset($entity->field_surname->value)) {
      $field_full_name .= ' ' . $entity->field_surname->value;
    }
    else if (isset($entity->last_name->value)) {
      $field_full_name .= ' ' . $entity->last_name->value;
    }

    $build['field_full_name'] = [
      '#type' => 'markup',
      '#markup' => trim($field_full_name),
    ];
  }
}

/**
 * Implements hook_entity_view_mode_alter().
 */
function infinite_base_entity_view_mode_alter(&$view_mode, EntityInterface $entity, $context) {
  if ($view_mode == 'presenter_home_selectable' && $entity->getEntityTypeId() === 'node') {
    $view_mode = 'presenter_half';
    if ($entity->hasField('field_hp_display_mode') && !$entity->get('field_hp_display_mode')->isEmpty()) {
      $selected_view_mode = $entity->get('field_hp_display_mode')->value;
      $view_mode = $selected_view_mode;
    }
  }
  else {
    if ($view_mode == 'teaser_selectable' && $entity->getEntityTypeId() === 'node') {
      $view_mode = 'teaser_square_m';
      if ($entity->hasField('field_teaser_display_mode') && !$entity->get('field_teaser_display_mode')->isEmpty()) {
        $selected_view_mode = $entity->get('field_teaser_display_mode')->value;
        $view_mode = $selected_view_mode;
      }
    }
  }
}

function infinite_base_theme() {
  return array(
    'author_teaser' => array(
      'template' => 'author-teaser',
      'variables' => array(
        'elements' => NULL,
        'name' => NULL,
        'author_id' => NULL,
        'author_forename' => NULL,
        'author_surname' => NULL,
        'author_url' => NULL,
        'author_picture' => NULL,
        'timestamp' => NULL,
        'use_absolute_date' => FALSE,
      ),
    ),

    'data_internal_url' => array(
      'variables' => array(
        'label' => NULL,
        'url' => NULL,
      ),
    ),

    'lazy_loading' => array(
      'variables' => array('lazy_loading_url' => NULL)
    ),
  );
}

function infinite_base_token_info() {

  // Node tokens.
  $info['tokens']['node']['root-channel'] = array(
    'name' => t('Root channel'),
    'description' => t("The root channel."),
  );
  $info['tokens']['term']['parents-all-join'] = array(
    'name' => t('Parents all join path'),
    'description' => t("Parents all join path"),
  );


  $nodeType = array(
    'name' => t('Nodes'),
    'description' => t('Tokens related to individual nodes.'),
    'needs-data' => 'node',
  );

  // Core tokens for nodes.
  $node['root-channel'] = array(
    'name' => t('Root channel'),
    'description' => t("The root channel."),
  );


  $termType = array(
    'name' => t('Terms'),
    'description' => t('Tokens related to individual terms.'),
    'needs-data' => 'term',
  );

  // Core tokens for nodes.
  $term['term-parents-all'] = array(
    'name' => t('Parents all'),
    'description' => t("Parents all."),
    'type' => 'array',
  );

  return array(
    'types' => array('node' => $nodeType, 'term' => $termType),
    'tokens' => array('node' => $node, 'term' => $term),
  );

  return $info;
}

function infinite_base_tokens($type, $tokens, array $data = array(), array $options = array(), BubbleableMetadata $bubbleableMetadata) {

  $replacements = array();

  if ($type == 'node' && !empty($data['node'])) {
    /** @var NodeInterface $node */
    $node = $data['node'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        // Simple key values on the node.
        case 'root-channel':

          if ($node->field_channel->entity) {
            $parents = \Drupal::entityTypeManager()
              ->getStorage('taxonomy_term')
              ->loadAllParents($node->field_channel->entity->id());

            $parents = array_values($parents);
            $countParents = count($parents);
            if (!empty($parents[$countParents - 2])) {
              $replacements[$original] = $parents[$countParents - 2]->getName();
            }
            else {
              $replacements[$original] = $node->field_channel->entity->getName();
            }
          }
          else {
            $replacements[$original] = '';
          }
          break;
      }
    }
  }


  if ($type == 'term' && !empty($data['term'])) {
    /** @var NodeInterface $node */
    $term = $data['term'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'term-parents-all':

          $replacements[$original] = infinite_base_get_all_parents($term);
          break;
      }
    }

    if ($parents_tokens = \Drupal::token()->findWithPrefix($tokens, 'term-parents-all')) {

      if ($parents = infinite_base_get_all_parents($term)) {

        $replacements += \Drupal::token()
          ->generate('array', $parents_tokens, array('array' => $parents), $options, $bubbleableMetadata);
      }
    }
  }


  return $replacements;
}

function infinite_base_get_all_parents($term) {

  $parents_tokens = [];

  if ($term) {
    $parents = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadAllParents($term->id());

    $parents = array_reverse(array_values($parents));

    if (count($parents) == 1) {
      $parents_tokens = [];
    } elseif (count($parents) > 1) {
      array_shift($parents);

      foreach($parents as $parent) {
        $parents_tokens[] = $parent->getName();
      }

    }


  }
  return $parents_tokens;
}

/**
 * Implements hook_preprocess_node().
 */
function infinite_base_preprocess_node(&$variables) {
  if ($variables['view_mode'] == 'full' || $variables['view_mode'] == 'lazyloading') {
    /* @var \Drupal\ad_integration\AdIntegration $advertisingService */
    $advertisingService = \Drupal::service('ad_integration');
    if (is_object($advertisingService)) {
      $variables['adsc_adunit1'] = $advertisingService->getAdUnit1();
      $variables['adsc_adunit2'] = $advertisingService->getAdUnit2();
      $variables['adsc_adunit3'] = $advertisingService->getAdUnit3();
      $variables['adsc_keyword'] = $advertisingService->getKeyword();
    }
  }
}

function infinite_base_page_attachments(array &$attachments) {
  /** Attaches the Harbourmaster ClientID, if module hm_newsletter is enabled */
  if (\Drupal::moduleHandler()->moduleExists('hm_newsletter')) {
    $configFactory = \Drupal::configFactory()->get('hm_newsletter.settings');
    $attachments['#attached']['drupalSettings']['hm_newsletter']['clientid'] = $configFactory->get('hm_client_id');
    $attachments['#attached']['drupalSettings']['hm_newsletter']['env'] = $configFactory->get('hm_environment');
    $attachments['#attached']['library'][] = 'hm_newsletter/base';
  }
}

/**
 * Replaces meta tag in html head with given content.
 *
 * @param type $name
 *
 * @param type $content
 *
 * @param array $attachments
 */
function infinite_base_replace_tag($name, $content, array &$attachments) {
  if (empty($attachments['#attached'])) {
    $attachments['#attached'] = [];
  }

  if (empty($attachments['#attached']['html_head'])) {
    $attachments['#attached']['html_head'] = [];
  }

  $index = infinite_base_find_tag($name, $attachments);

  if ($index > -1) {
    $attachments['#attached']['html_head'][$index][0]['#attributes']['content'] = $content;
  }
  else {
    $attachments['#attached']['html_head'][] = [
      0 => [
        '#attributes' => ['name' => $name, 'content' => $content],
        '#tag' => 'meta',
      ],
      1 => 'description',
    ];
  }
}

/**
 * Finds the index of a meta tag in the html head.
 *
 * @param type $name
 *
 * @param array $attachments
 *
 * @return int
 */
function infinite_base_find_tag($name, array &$attachments) {
  foreach ($attachments['#attached']['html_head'] as $index => $attachment) {
    if ($attachment[1] == $name) {
      return $index;
    }
  }
  return -1;
}

/**
 * Helper function to get promote states as a flat array.
 *
 * @param EntityInterface $entity
 * @return array
 */
function _infinite_base_flat_promote_states(EntityInterface $entity) {
  if ($entity->hasField('field_promote_states')) {
    $promote_states = array_map(function($el) {
      return $el['value'];
    }, $entity->field_promote_states->getValue());
    return $promote_states;
  }
  return FALSE;
}
